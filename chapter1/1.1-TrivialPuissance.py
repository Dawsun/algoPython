

def calculPuissance(x, n):

    p = x

    for i in range(1, n):
        x *= p
    
    return x


def main():
    #main function
    x = int(input("X: "))
    n = int(input("N: "))

    result = calculPuissance(x, n)

    print("Resultat de " + str(x) + "^" + str(n) + ": " + str(result))


if __name__ == "__main__":
    main()