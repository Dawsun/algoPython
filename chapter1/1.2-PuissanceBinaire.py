

def getBinaryOf(n):
    binNumber = []
    reste = 1

    while n != 0:
        
        binNumber.append(n%2)
        n = n//2
    
    return reverseArray(binNumber)

def reverseArray(l1):
    l2 = []
    for i in range(0, len(l1)):
        l2.append(l1[len(l1)-(i+1)])
    return l2

# Méthode = Parcourir le nombre binaire en commencant par le deuxième
# Si c'est un 0 on mets puiss au carré et si c'est un 1 on mets au carré
# et on multiplie par x
def puissance(x, n):
    nBin = getBinaryOf(n)
    puiss = x
    print("Nombre binaire: " + str(nBin))
    for i in nBin[1:]:
        puiss *= puiss
        if i == 1:
            puiss = puiss * x
    print("X: " + str(puiss))
        
    

def main():
    #main function
    puissance(10, 10)

if __name__ == "__main__":
    main()