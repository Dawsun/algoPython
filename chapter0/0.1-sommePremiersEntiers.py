def somme(n):
    s = 0
    for i in range(1, n):
        s += i
    return s

def main():
    ask = int(input("Somme de : "));
    result = somme(ask)
    print("Somme des " + str(ask) + " premiers entiers: " + str(result))

if __name__ == "__main__":
    main()