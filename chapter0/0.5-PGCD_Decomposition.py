def ppd(n):
    i = 2

    if n < 2:
        return n

    while n % i != 0:
        i += 1
    
    return i

def decomposition(n):
    factors = []

    i = 2

    while i != 1:
        i = ppd(n)
        factors.append(i)
        n = n//i

    

    return factors

def commun(l1, l2):
    l3 = []
    
    
    while len(l1) != 0 and len(l2) != 0:
        if l1[0] == l2[0]:
            l3.append(l1[0])
            l1.pop(0)
            l2.pop(0)
        elif l1[0] < l2[0]:
            l1.pop(0)
        else:
            l2.pop(0)
        
        #print("len(l1): " + str(len(l1)) + " - len(l2): " + str(len(l2))) -> debug invalid index
    
    return l3

def produitTab(l1):
    result = 1
    for i in range(0, len(l1)):
        result *= l1[i]
    return result


def pgcd(a, b):
    l1 = decomposition(a)
    l2 = decomposition(b)
    l3 = commun(l1, l2)

    # print("l1: " + str(l1))
    # print("l2: " + str(l2))
    print("l3: " + str(l3))

    print("PGCD: " + str(produitTab(l3)))


def main():
    #main function
    a = int(input("A: "))
    b = int(input("B: "))

    pgcd(a, b)

if __name__ == "__main__":
    main()