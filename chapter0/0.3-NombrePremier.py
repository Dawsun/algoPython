

def isPrimary(n):
    i = 2

    if n < 2:
        return False

    while n % i != 0:
        i += 1
    return (i==n)

def main():
    #main function
    ask = int(input("Entrez un nombre: "))
    
    if isPrimary(ask):
        print("Nombre premiers...")
    else:
        print("Nombre non premiers...")

if __name__ == "__main__":
    main()