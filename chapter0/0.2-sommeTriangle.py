def somme(n):
    s = 0
    for i in range(1, n):
        s += i
    return s

def main():
    ask = int(input("Répétition somme des sommes: "));
    s = 0;
    for i in range(0, ask+1):
        for j in range(0, i+1):
            s += j;
    print("Somme des sommes à " + str(ask) + " répétitions: " + str(s))



if __name__ == "__main__":
    main()