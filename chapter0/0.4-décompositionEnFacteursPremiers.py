
def ppd(n):
    i = 2

    if n < 2:
        return n

    while n % i != 0:
        i += 1
    
    return i

def decomposition(n):
    factors = []

    i = 2

    while i != 1:
        i = ppd(n)
        print("n: " + str(n))
        factors.append(i)
        n = n//i

    

    print("Facteurs premiers: " + str(factors))





def main():
    #main function
    decomposition(int(input("Nombre: ")))

if __name__ == "__main__":
    main()