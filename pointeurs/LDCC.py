import random

class Cellule:
    def __init__(self, info=0, suivant=None, precedent=None):
        self.info = info
        self.suivant = suivant
        self.precedent = precedent
    
class LDCC:
    def __init__(self):
        self.debut = None
        self.fin = None
        self.sens = 0
    
    def insertion_fin(self, valeur):
        if self.debut == None:
            c = Cellule(valeur)
            self.debut = c
            self.fin = c
            c.suivant = c
            c.precedent = c
        else:
            c = Cellule(valeur)
            c.suivant = self.debut
            c.precedent = self.fin
            self.fin.suivant = c
            self.debut.precedent = c
            self.fin = c
    
    def en_avant(self):
        if self.debut != None:
            self.debut = self.debut.suivant
            self.fin = self.fin.suivant
    def en_arriere(self):
        if self.debut != None:
            self.debut = self.debut.precedent
            self.fin = self.fin.precedent
    
    def change_sens(self):
        if self.sens == 0:
            self.sens = 1
        else:
            self.sens = 0
    
    def affiche_debut(self):
        print(self.debut.info)
    
    def affiche_fin(self):
        print(self.fin.info)
    
    def affiche_tous(self):
        if self.debut != None:
            currentCellule = self.debut
            while currentCellule != self.fin:
                print(currentCellule.info)
                currentCellule = currentCellule.suivant
            print(self.fin.info)
    
    def supprime(self):
        if self.debut != None:
            if self.debut == self.fin:
                self.debut = None
                self.fin = None
            else:
                self.debut = self.debut.suivant
                self.debut.precedent = self.fin
                self.fin.suivant = self.debut

    def longeur(self):
        l = 0
        if self.debut != None:
            currentCellule = self.debut
            while currentCellule != self.fin:
                currentCellule = currentCellule.suivant
                l += 1
            l += 1
        return l
    
    def supprimeInfo(self, info):
        if self.debut != None:
            currentCellule = self.debut
            while currentCellule != self.fin:
                self.en_avant()
                if self.debut.info == info:
                    self.supprime()
                    return
                currentCellule = currentCellule.suivant
            if self.fin.info == info:
                self.en_avant()
                self.supprime()
            
            

############### Test manipulation d'une chaine #################
def testManipChaine():
    l = LDCC()
    l.insertion_fin(1)
    l.insertion_fin(2)
    l.insertion_fin(3)
    print("#On affiche toute la liste")
    l.affiche_tous()
    print(l.longeur())
    print("#On supprime le premier élément")
    l.supprime()
    print("Taille de la liste : " + str(l.longeur()))

############### Les chaises musicales ##########################


##################### Le jeu ########################################
def jeu():
    # Création de la liste chainée
    l = LDCC()
    # Ajouts des membres de la partie
    l.insertion_fin("Paul")
    l.insertion_fin("Marie")
    l.insertion_fin("Pierre")
    l.insertion_fin("Nathalie")
    l.insertion_fin("Philippe")
    l.insertion_fin("Romain")
    l.insertion_fin("Brigitte")
    l.insertion_fin("Raymond")
    l.insertion_fin("Valérie")
    l.insertion_fin("Pascal")

    tour = 1
    while l.longeur() > 1:
        n = random.randrange(1, 20) # On génère un nombre aléatoire
        if tour%2 == 0:
            for i in range(0, n):
                l.en_avant()
            print("[INFO JEU] Tour " + str(tour) + " - Personne éliminé: " + str(l.debut.info)) # On informe
            l.supprime()
            tour += 1
        else:
            for i in range(0, n):
                l.en_arriere()
            print("[INFO JEU] Tour " + str(tour) + " - Personne éliminé: " + str(l.debut.info)) # On informe
            l.supprime()
            tour += 1
            

    print("[INFO JEU] Le grand gagnant est: " + str(l.debut.info))
    l.affiche_tous()

############################### Test supprimeInfo ##############################

def supprimeInfoTest():
    l = LDCC()
    l.insertion_fin(1)
    l.insertion_fin(2)
    l.insertion_fin(3)
    l.insertion_fin(4)
    l.insertion_fin(5)
    print("# Liste complète:")
    l.affiche_tous()
    toDelete = int(input("Valeur à supprimer: "))
    l.supprimeInfo(toDelete)
    print("#List après suppression de " + str(toDelete))
    l.affiche_tous()


if __name__ == "__main__":
    print("#### Les listes chainée ####")
    print("1) Test manipulation de liste chainée")
    print("2) Chaises musicales")
    print("3) supprimeInfo")

    choice = int(input("Votre choix: "))

    if choice == 1:
        testManipChaine()
    elif choice == 2:
        jeu()
    elif choice == 3:
        supprimeInfoTest()
    else:
        print("Choix invalide")