LoopNumber = 0

def tri_insertion(l):
    global LoopNumber
    for j in range(1, len(l)):  
        nb = l[j]
        i = j-1
        LoopNumber += 1
        while l[i] > nb and i >= 0:
            l[i+1] = l[i]
            i = i-1
            LoopNumber += 1
        l[i+1] = nb


def main():
    #main function
    l = [9, 1, 8, 2, 7, 4]
    print(l)
    tri_insertion(l)
    print(l)

    print("Nombre de tour de boucle: " + str(LoopNumber))

if __name__ == "__main__":
    main()