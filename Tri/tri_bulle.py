from random import *



def genRandomList(length):
    loopNumber = 0;
    l = []
    for i in range(0, length):
        l.append(randrange(100000))
        loopNumber += 1
    return l, loopNumber




def triBulle(l):
    loopNumber = 0
    move = True

    while move:
        move = False
        for i in range(0, (len(l)-1)):

            if l[i] > l[i+1]:
                #change
                l[i], l[i+1] = l[i+1], l[i]
                move = True
                loopNumber += 1
    
    return l, loopNumber







def main():
    #main function
    l, loopNumber = genRandomList(1000)
    print("Liste: " + str(l) + " - loopNumber: " + str(loopNumber));



    l1, loopNumberTri = triBulle(l)
    print("Liste trié: " + str(l1) + " - loopNumber: " + str(loopNumberTri));



if __name__ == "__main__":
    main()