
def getMax(l):
    m = 0
    for i in l:
        if l[i] > m:
            m = l[i]
    return m

# On intervertit le plus grand membre avec le dernier membre 
def tri_selection(l):
    n = len(l)
    for i in range(n-1, 0, -1):
        t = l[i]
        l[i] = l[getMax(l[0:i])]
        l[getMax(l[0:i])] = t
        

        


def main():
    #main function
    l = [9, 1, 8, 2, 7, 4]
    print(l)
    tri_selection(l)
    print(l)

if __name__ == "__main__":
    main()