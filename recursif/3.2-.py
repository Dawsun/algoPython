def ppd(n):
    i = 2

    if n < 2:
        return n

    while n % i != 0:
        i += 1
    
    return i

def isPrimary(n):
    i = 2

    if n < 2:
        return False

    while n % i != 0:
        i += 1
    return (i==n)

def puissance(x, n):
    if n == 0:
        return 1
    elif n == 1:
        return x
    elif isPrimary(n):
        return x*puissance(x, n-1)
    elif p == ppd(n):
        q = n/p
        return puissance(puissance(x, n), q)


def main():
    #main function
    n = int(input("n: "))
    p = int(input("p: "))

    print("n^p: " + str(puissance(n, p)))

if __name__ == "__main__":
    main()