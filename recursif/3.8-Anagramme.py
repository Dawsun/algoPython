def anagramme(word):
    tab = []
    if len(word) == 1:
        return [word]
    for i in range(len(word)):
        firstLetter = word[i]
        subWord = word.replace(word[i], "")
        anaSub = anagramme(subWord)
        for sub in anaSub:
            anag = firstLetter + sub
            tab.append(anag)
    return tab

def main():
    #main function
    tab = []
    word = "aze"
    anag = anagramme(word)
    print(anag)
    


if __name__ == "__main__":
    main()