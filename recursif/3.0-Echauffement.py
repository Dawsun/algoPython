

def puissanceRecur(x, n):#Puissance récursive
    if n == 0:
        return 1
    return x*puissanceRecur(x, n-1)

def factorielleRecur(x):
    if x == 0:
        return 1
    return x*(x-1)


def main():
    #main function

    #========== Puissance récursive ==============
    # ask = int(input("Nombre: "))
    # n = int(input("Puissance: "))
    # print("Carré du nombre: " + str(puissanceRecur(ask, n)))

    #========= Factorielle récursive =============
    ask = int(input("Nombre facto: "))
    print("Factorielle de " + str(ask) + ": " + str(factorielleRecur(ask)))




if __name__ == "__main__":
    main()