def palindrome(chaine):
    if chaine[0] == chaine[-1]:
        palindrome(chaine[1:-2])
        return True
    else:
        return False


def main():
    #main function
    ask = str(input("Mot: "))

    print("Palindrome: " + str(palindrome(ask)))


if __name__ == "__main__":
    main()