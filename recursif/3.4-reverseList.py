def reverseList(l):
    if len(l) <= 0:
        return;
    print(l[-1], end = '')
    reverseList(l[0:-1])

def main():
    #main function
    l = "1234567"

    print(l)

    reverseList(l)

if __name__ == "__main__":
    main()