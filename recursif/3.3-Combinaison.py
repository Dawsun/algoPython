def combinaison(n, p):
    if p == 0 or p == n:
        return 1
    else:
        return combinaison(n-1, p)+combinaison(n-1, p-1)

def main():
    #main function
    n = int(input("N: "))
    p = int(input("P: "))

    print("(n, p) = " + str(combinaison(n, p)))

if __name__ == "__main__":
    main()